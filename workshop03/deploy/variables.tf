variable DO_token {
    type = string
    sensitive = true
}

variable DO_region {
    type = string
    default = "sgp1"
}

variable DO_size {
    type = string
    default = "s-1vcpu-2gb"
}

variable DO_image {
    type = string
    default = "mydroplet"
}

variable ssh_key_name {
    type = string
    default = "ssh-key-for-course"
}

variable private_key {
    type = string
    default = "id_rsa"
}

variable "codeserver_password" {
  type      = string
  sensitive = true
}
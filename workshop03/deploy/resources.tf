data digitalocean_ssh_key mykey {
    name = var.ssh_key_name
}

data digitalocean_droplet_snapshot snapshot {
  name_regex  = "mydroplet"
  region      = "sgp1"
  most_recent = true
}

resource digitalocean_droplet codeserver {
    name = "codeserver-ws3"
    image = data.digitalocean_droplet_snapshot.snapshot.id
    region = var.DO_region
    size = var.DO_size
    ssh_keys = [ data.digitalocean_ssh_key.mykey.id ]
}


output codeserver_ip {
    description = "codeserver IP"
    value = digitalocean_droplet.codeserver.ipv4_address
}

resource local_file inventory {
    filename = "inventory.yaml"
    content = templatefile("inventory.yaml.tftpl", {
        private_key = var.private_key
        codeserver_ip = digitalocean_droplet.codeserver.ipv4_address
        codeserver_domain: "codeserver-${digitalocean_droplet.codeserver.ipv4_address}.nip.io"
        codeserver_password: var.codeserver_password
    })
    file_permission = 644
}
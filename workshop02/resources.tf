data digitalocean_ssh_key mykey {
    name = var.ssh_key_name
}

resource digitalocean_droplet codeserver {
    name = "codeserver"
    image = var.DO_image
    region = var.DO_region
    size = var.DO_size
    ssh_keys = [ data.digitalocean_ssh_key.mykey.id ]
}


output codeserver_ip {
    description = "codeserver IP"
    value = digitalocean_droplet.codeserver.ipv4_address
}

resource local_file inventory {
    filename = "inventory.yaml"
    content = templatefile("inventory.yaml.tftpl", {
        private_key = var.private_key
        codeserver_ip = digitalocean_droplet.codeserver.ipv4_address
        codeserver_domain: "codeserver-${digitalocean_droplet.codeserver.ipv4_address}.nip.io"
        codeserver_password: var.codeserver_password
    })
    file_permission = 644
}
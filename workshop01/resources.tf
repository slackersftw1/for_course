resource docker_image ws1 {
    name = "chukmunnlee/dov-bear:v2"
}

data digitalocean_ssh_key mykey {
    name = var.ssh_key_name
}

resource docker_container ws1_container {
    count = var.replicas
    name = "dov-${count.index}"
    image = docker_image.ws1.image_id
    ports {
        internal = 3000
        external = 30000 + count.index
    }

    env = [
        "INSTANCE_NAME=dov-${count.index}"
    ]
}

resource digitalocean_droplet nginx {
    name = "nginx"
    image = var.DO_image
    region = var.DO_region
    size = var.DO_size
    ssh_keys = [ data.digitalocean_ssh_key.mykey.id ]

     connection {
        type = "ssh"
        user = "root"
        private_key = file("id_rsa")
        host = self.ipv4_address
    }

    provisioner remote-exec {
        inline = [
            "apt update",
            "apt install -y nginx",
            "systemctl enable nginx",
            "systemctl start nginx"
        ]
    }

    provisioner file {
        source = "./${local_file.nginx_conf.filename}"
        destination = "/etc/nginx/nginx.conf"
    }

     provisioner remote-exec {
        inline = [
            "systemctl restart nginx"
        ]
    }
}

resource local_file nginx_conf {
    filename = "nginx.conf"
    content = templatefile("nginx.conf.tftpl", {
        docker_host = "174.138.26.58"
        container_ports = local.ports
    })
}

locals {
    ports = [for p in docker_container.ws1_container[*].ports: p[0].external ]
}

output ws1_container_ports {
  description = "Output for ports"
  value = local.ports
}

output nginx_ip {
    description = "nginx IP"
    value = digitalocean_droplet.nginx.ipv4_address
}
